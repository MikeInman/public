package com.app;

public class BowlingFrame {
	int scores[] = new int[3];

	BowlingFrame() {
		initializeScores();
	}

	private void initializeScores() {
		scores[0] = 0;
		scores[1] = 0;
		scores[2] = 0;
	}

	public void setScores(int firstRoll, int secondRoll) {
		setScores(firstRoll, secondRoll, 0);
	}

	public void setScores(int firstRoll, int secondRoll, int thirdRoll) {
		scores[0] = firstRoll;
		scores[1] = secondRoll;
		scores[2] = thirdRoll;
	}

	public boolean isUnScored() {
		return scores[0] < 0;
	}

	public int getFirstRoll() {
		return scores[0];
	}

	public int getSecondRoll() {
		return scores[1];
	}

	public int getBaseScore() {
		if (isUnScored()) {
			return 0;
		}
		return scores[0] + scores[1];
	}

	public int getCombinedScore() {
		if (isUnScored()) {
			return 0;
		}
		return scores[0] + scores[1] + scores[2];
	}

	public int getScore(BowlingFrame nextFrame, BowlingFrame secondFrame) {
		if (isSpare()) {
			return getCombinedScore() + nextFrame.getFirstRoll();
		} else if (isStrike()) {
			if (nextFrame.isStrike()) {
				return getCombinedScore() + nextFrame.getBaseScore() + secondFrame.getFirstRoll();
			}
			return getCombinedScore() + nextFrame.getBaseScore();
		}
		return getCombinedScore();
	}

	public boolean isSpare() {
		return !isStrike() && getBaseScore() == 10;
	}

	public boolean isStrike() {
		// TODO Auto-generated method stub
		return scores[0] == 10;
	}

	
	public String toString(){
		StringBuilder framesString = new StringBuilder();
		
		if (isStrike()){
			framesString.append("X");
		}
		else if (isSpare()){
			framesString.append(scores[0]);
			framesString.append("/");
		}
		else {
			framesString.append(scores[0]);
			framesString.append(scores[1]);
			if (scores[2] != 0){
				framesString.append(scores[2]);
			}
		}
		
		framesString.append(" ");
		return framesString.toString();
	}
}
