package com.app;

public class ScoreSheet {
	BowlingFrame[] frames = new BowlingFrame[10];

	ScoreSheet() {
		for (int i = 0; i < frames.length; i++) {
			frames[i] = new BowlingFrame();
		}
	}

	public int getFinalScore() {
		int finalScore = 0;
		for (int i = 0; i < frames.length; i++) {
			finalScore += getBowlingFrameScore(i);
		}
		return finalScore;
	}

	/**
	 * @param i
	 *            frames 1-10
	 * @return
	 */
	public BowlingFrame getFrame(int i) {
		return frames[i];
	}

	/**
	 * @param i
	 *            frames 1-10
	 * @return
	 */
	public int getBowlingFrameScore(int frame) {
		BowlingFrame nextFrame = (frame + 1 < frames.length) ? frames[frame + 1] : new BowlingFrame();
		BowlingFrame secondNextFrame = (frame + 2 < frames.length) ? frames[frame + 2] : new BowlingFrame();

		return frames[frame].getScore(nextFrame, secondNextFrame);

	}

	public int getAggregatedFrameScore(int frame) {
		int finalScore = 0;
		if (getFrame(frame).isUnScored())
			return 0;
		for (int i = 0; i <= frame; i++) {

			finalScore += getBowlingFrameScore(i);
		}
		return finalScore;
	}
	
	public void setFrame(BowlingFrame aFrame, int aFrameNumber){
		frames[aFrameNumber-1] = aFrame;
		
	}

	
	public String getFramesString(){
		StringBuilder framesString = new StringBuilder();
		
		for (BowlingFrame frame : this.frames){
			framesString.append(frame.toString());
			framesString.append(" ");
		}
		return framesString.toString();
	}
}
