package com.app;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

import java.util.HashMap;
import java.util.Map;

import org.dalesbred.Database;

import spark.ModelAndView;
import spark.Request;
import spark.Spark;
import spark.template.velocity.VelocityTemplateEngine;

public class BowlingApp {
	public static void main(String[] args) {
		port(4567);
		Spark.staticFileLocation("/public");

		Database db = getDatabase();
		
		get("/total_score_for_game_id", (request, response) -> {
			String gameId = request.queryParams("gameId");
			switch (gameId) {
			case "456":
				return "15";
			case "842":
				return "300";
			}
			return "200";
		});

		get("/", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("showreadonly", false);

			return new ModelAndView(model, "/velocity/InputScoresheet.vm");
		}, new VelocityTemplateEngine());

		post("/", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("showreadonly", false);

			// TODO Sheet is built up from the UI form
			ScoreSheet sheet = getSheetFromRequest(request);
		    db.update( "insert into games(id, rolls) values( ?, ? )", request.queryParams("gameId"), sheet.getFramesString() );
			model.put("sheet", sheet);

			return new ModelAndView(model, "/velocity/InputScoresheet.vm");
		}, new VelocityTemplateEngine());

		get("/game_code", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("showreadonly", false);

			return new ModelAndView(model, "/velocity/BowlingComScore.vm");
		}, new VelocityTemplateEngine());

		post("/game_code", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("showreadonly", true);

			// TODO Sheet is created from the DataService. the ID from the form
			// should be on request.queryParams("gameId")
			ScoreSheet sheet = new ScoreSheet();
			model.put("sheet", sheet);

			return new ModelAndView(model, "/velocity/BowlingComScore.vm");
		}, new VelocityTemplateEngine());
	}

	protected static Database getDatabase() {
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return Database.forUrlAndCredentials( "jdbc:h2:tcp://localhost:9092/~/acme;" +
	      "INIT=CREATE TABLE IF NOT EXISTS GAMES( id VARCHAR(255), rolls VARCHAR(64) );"
	      , "sa", "sa" );
	}

	private static ScoreSheet getSheetFromRequest(Request request) {
		ScoreSheet sheet = new ScoreSheet();
		for (int i = 0; i < 10; i++) {
			sheet.getFrame(i).setScores(getRollFromFrame(request, i, 0), getRollFromFrame(request, i, 1));
		}
		return sheet;
	}

	private static int getRollFromFrame(Request request, int frame, int roll) {
		String s = request.queryParams("roll_" + frame + "_" + roll);
		if (s.isEmpty())
			return 0;
		return Integer.parseInt(s);
	}
}
