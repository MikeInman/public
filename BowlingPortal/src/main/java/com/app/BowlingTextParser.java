package com.app;

public class BowlingTextParser {
	private String recordToParse;

	public BowlingTextParser(String aRecordToParse) {
		recordToParse = aRecordToParse;
	}

	public ScoreSheet parseScoreSheet() {
		ScoreSheet sheet = new ScoreSheet();
		String[] frameDataArray = recordToParse.split(" ");
		if (frameDataArray.length < 10) {
			// FIXME: Make this a clearer error message, or another exception
			// type
			throw new RuntimeException("Invalid String entry");
		}
		for (int i = 0; i < 10; i++) {
			String frameData = frameDataArray[i];
			BowlingFrame frame = createFrameFromString(frameData);
			// Check for bonus throws
			if (i == 9 && frameDataArray.length == 11) {
				BowlingFrame bonusFrame = createFrameFromString(frameDataArray[10]);
				if (frame.isSpare()) {
					frame.setScores(frame.getFirstRoll(), frame.getSecondRoll(), bonusFrame.getFirstRoll());
				}
				if (frame.isStrike()) {
					frame.setScores(frame.getFirstRoll(), bonusFrame.getFirstRoll(), bonusFrame.getSecondRoll());
				}
			}
			sheet.setFrame(frame, i + 1);
		}
		return sheet;
	}

	private BowlingFrame createFrameFromString(String frameData) {
		int[] throwValues = convertStringToThrowValues(frameData);

		BowlingFrame frame = new BowlingFrame();
		frame.setScores(throwValues[0], throwValues[1]);

		return frame;

	}

	public int[] convertStringToThrowValues(String aStringValue) {
		int[] returnValues = new int[2];
		char firstChar = aStringValue.charAt(0);
		// Process the first character
		if (firstChar == 'X') {
			returnValues[0] = 10;
			returnValues[1] = 0;
			return returnValues;
		} else if (firstChar == '-') {
			returnValues[0] = 0;
		} else {
			returnValues[0] = Character.getNumericValue(firstChar);
		}

		// Process the second character. If first throw was a strike, won't
		// reach this code
		char secondChar;
		if (aStringValue.length() == 2) {
			secondChar = aStringValue.charAt(1);
		} else {
			secondChar = '0';
		}

		if (secondChar == '/') {
			returnValues[1] = 10 - returnValues[0];
		} else if (secondChar == '-') {
			returnValues[1] = 0;
		} else {
			returnValues[1] = Character.getNumericValue(secondChar);
		}

		return returnValues;
	}

}
