Feature: Manual input
	To allow a user to input bowling scores and receive their total score

	Scenario: A hard coded input
		Given the user wants to input their scores manually
		When they input all 1's
		Then they should have a final score of 20