package com.app;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BowlingScoreTest {
	ScoreSheet sheet;

	@Before
	public void beforeEach() {
		sheet = new ScoreSheet();
	}

	@Test
	public void newBowlingGameHasScore0() {
		assertEquals(0, sheet.getFinalScore());
	}

	@Test
	public void bowlingGameWithNoSparesAndStrikes() {
		setFramesForScore30WithoutSparesOrStrikes();

		assertEquals(30, sheet.getFinalScore());
	}

	@Test
	public void gameWithSingleSpare() {
		setFramesForScore30WithoutSparesOrStrikes();
		sheet.getFrame(1).setScores(1, 9);

		assertEquals(38, sheet.getFinalScore());

	}

	@Test
	public void gameWithSingleStrike() {
		setFramesForScore30WithoutSparesOrStrikes();
		sheet.getFrame(1).setScores(10, 0);

		assertEquals(40, sheet.getFinalScore());
	}

	@Test
	public void getAggregatedFrameScore() {
		setFramesForScore30WithoutSparesOrStrikes();

		assertEquals(3, sheet.getAggregatedFrameScore(0));
		assertEquals(6, sheet.getAggregatedFrameScore(1));
		assertEquals(9, sheet.getAggregatedFrameScore(2));
		assertEquals(12, sheet.getAggregatedFrameScore(3));
		assertEquals(15, sheet.getAggregatedFrameScore(4));
	}

	public void setFramesForScore30WithoutSparesOrStrikes() {
		sheet.getFrame(0).setScores(1, 2);
		sheet.getFrame(1).setScores(1, 2);
		sheet.getFrame(2).setScores(1, 2);
		sheet.getFrame(3).setScores(1, 2);
		sheet.getFrame(4).setScores(1, 2);
		sheet.getFrame(5).setScores(1, 2);
		sheet.getFrame(6).setScores(1, 2);
		sheet.getFrame(7).setScores(1, 2);
		sheet.getFrame(8).setScores(1, 2);
		sheet.getFrame(9).setScores(1, 2);
	}

	@Test
	public void gameWithPerfectScore() {
		rollPerfectGame();

		assertEquals(300, sheet.getFinalScore());
	}

	@Test
	public void getSpecificFrameScore() {
		rollPerfectGame();

		assertEquals(30, sheet.getBowlingFrameScore(3));
	}

	public void rollPerfectGame() {
		sheet.getFrame(0).setScores(10, 0);
		sheet.getFrame(1).setScores(10, 0);
		sheet.getFrame(2).setScores(10, 0);
		sheet.getFrame(3).setScores(10, 0);
		sheet.getFrame(4).setScores(10, 0);
		sheet.getFrame(5).setScores(10, 0);
		sheet.getFrame(6).setScores(10, 0);
		sheet.getFrame(7).setScores(10, 0);
		sheet.getFrame(8).setScores(10, 0);
		sheet.getFrame(9).setScores(10, 10, 10);
	}

}
