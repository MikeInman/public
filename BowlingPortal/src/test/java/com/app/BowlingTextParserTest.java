package com.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class BowlingTextParserTest {
	
	
	private static final String SCORES_NO_SPECIALS = "-5 14 23 5- -- -- -- -- -- --";
	private static final String SCORES_WITH_SPARE = "-5 14 23 5- 2/ -- -- -- -- --";
	private static final String SCORES_WITH_STRIKE = "-5 14 23 5- X -- -- -- -- --";
	private static final String SCORES_WITH_GUTTERBALLS = "-5 -- -- -- -- -- -- -- -- --";
	private static final String SCORES_WITH_SINGLE_EXTRA_THROW = "-- -- -- -- -- -- -- -- -- 4/ 5";
	private static final String SCORES_WITH_TWO_EXTRA_THROWS = "-- -- -- -- -- -- -- -- -- X 54";
	private static final String SCORES_WITH_ONLY_9_FRAMES = "-- -- -- -- -- -- -- -- --";
	
	
	@Test
	public void confirmSheetHasCorrectScoreNoSpecials() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_NO_SPECIALS);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(20,sheet.getFinalScore());
	}
	
	@Test
	public void confirmSheetHasCorrectScoreWithSpare() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_SPARE);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(30,sheet.getFinalScore());
	}
	
	@Test
	public void confirmSheetHasCorrectScoreWithStrike() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_STRIKE);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(30,sheet.getFinalScore());
	}
	
	@Test
	public void confirmCanHandleGutterBall() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_GUTTERBALLS);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(5,sheet.getFinalScore());
	}
	
	@Test
	public void confirmExtraSingleThrowCounted() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_SINGLE_EXTRA_THROW);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(15,sheet.getFinalScore());
	}
	
	@Test
	public void confirmExtraTwoThrowsCounted() {
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_TWO_EXTRA_THROWS);
		ScoreSheet sheet = parser.parseScoreSheet();
		assertEquals(19,sheet.getFinalScore());
	}
	
	@Test (expected = RuntimeException.class)
	public void confirmInvalidArrayThrowsException(){
		BowlingTextParser parser = new BowlingTextParser(SCORES_WITH_ONLY_9_FRAMES);
		ScoreSheet sheet = parser.parseScoreSheet();
	}

}
