package com.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class BowlingFrameTest {
	BowlingFrame frame;
	BowlingFrame nextFrame;
	BowlingFrame secondNextFrame;

	@Before
	public void beforeEach() {
		frame = new BowlingFrame();
		nextFrame = new BowlingFrame();
		secondNextFrame = new BowlingFrame();
	}

	@Test
	public void getFrameScoreWithoutSpareOrStrike() {
		frame.setScores(3, 4);

		assertEquals(7, frame.getBaseScore());
	}

	@Test
	public void isStrike() {
		frame.setScores(10, 0);

		assertTrue(frame.isStrike());
	}

	@Test
	public void spareIsNotStrike() {
		frame.setScores(10, 0);

		assertFalse(frame.isSpare());
	}

	@Test
	public void isSpare() {
		frame.setScores(3, 7);

		assertTrue(frame.isSpare());
	}

	@Test
	public void getFrameScoreWithSpare() {
		frame.setScores(3, 7);
		BowlingFrame nextFrame = new BowlingFrame();
		nextFrame.setScores(4, 5);

		assertEquals(14, frame.getScore(nextFrame, secondNextFrame));
	}

	@Test
	public void getFrameScoreWithStrikeAndNonSpecialNextFrame() {
		frame.setScores(10, 0);
		nextFrame.setScores(3, 4);

		assertEquals(17, frame.getScore(nextFrame, secondNextFrame));
	}

	@Test
	public void getStrikeWithStrikeInNextFrame() {
		frame.setScores(10, 0);
		nextFrame.setScores(10, 0);
		secondNextFrame.setScores(3, 4);

		assertEquals(23, frame.getScore(nextFrame, secondNextFrame));
	}
}
