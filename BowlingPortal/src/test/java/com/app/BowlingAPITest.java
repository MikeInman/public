package com.app;

import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;

public class BowlingAPITest {

	private Connection getConnection() throws SQLException {
		Connection connection = null;

		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		connection = DriverManager.getConnection("jdbc:h2:tcp://localhost:9092/~/acme", "sa", "sa");

		return connection;
	}

	private void runScript(String fileName) throws SQLException {
		Reader script = new InputStreamReader(this.getClass().getResourceAsStream(fileName));
		RunScript.execute(this.getConnection(), script);
	}

	@Before
	public void before() throws SQLException {
		runScript("api-setup.sql");
	}

	@After
	public void after() throws SQLException {
		runScript("api-teardown.sql");
	}
}
