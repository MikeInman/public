package feature.manual;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.junit.Test;

public class TotalScoreAPITest {
	@Test
	public void testTotalScoreAPI() throws ClientProtocolException, IOException {
		String content842 = Request.Get("http://localhost:4567/total_score_for_game_id?gameId=842").connectTimeout(5)
				.socketTimeout(5).execute().returnContent().asString();

		assertEquals("300", content842);
	}
}
