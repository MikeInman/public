package feature.manual;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ManualInputSteps {
	int num = 22;

	@Given("^the user wants to input their scores manually$")
	public void inputGameBoardManually() {
		num = 12;
	}

	@When("^they input all 1's$")
	public void allRollsHit1Pin() {
		num += 2;
	}

	@Then("^they should have a final score of (\\d+)$")
	public void verifyFinalScore(final int expectedFinalScore) {
		assertThat(num, equalTo(expectedFinalScore));
	}
}
