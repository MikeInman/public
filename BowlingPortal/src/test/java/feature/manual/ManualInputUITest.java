package feature.manual;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ManualInputUITest {
	private static WebDriver driver;
	WebElement element;

	@BeforeClass
	public static void openBrowser() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void canManuallyInputScore() {
		System.out.println("Starting test " + new Object() {
		}.getClass().getEnclosingMethod().getName());
		driver.get("http://localhost:4567/");
		driver.findElement(By.xpath(".//input[@name=\"roll_0_0\"]")).sendKeys("1");
		driver.findElement(By.id("submit_score_sheet")).click();
		try {
			element = driver.findElement(By.xpath(".//*[@id='total_score']"));
		} catch (Exception e) {
		}
		Assert.assertNotNull(element);
		assertEquals("1", element.getText());
		System.out.println("Ending test " + new Object() {
		}.getClass().getEnclosingMethod().getName());
	}

	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}

}
